package net.uniqueValue;

import java.util.Scanner;
import java.util.HashSet;

public class UniqueValue {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Введите количество элементов: ");
        int count = scanner.nextInt();

        String[] array = new String[count]; //массив повторяющихся значений

        System.out.println("Заполните массив. " + count + " элемента(ов)");
        for (int i = 0; i < count; i++) {
            array[i] = scanner.next();
        }

        HashSet<String> sequence = new HashSet<String>(); //Set - интерфейс "множество" позволяет хранить ТОЛЬКО уникальыне значения
        for (int i = 0; i < count; i++) {
            sequence.add(array[i]); //добавляем элемент в множество, если его там ещё нет
        }
        System.out.println(sequence);
    }
}
